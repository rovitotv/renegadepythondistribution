.. Renegade Python Distribution documentation master file, created by
   sphinx-quickstart on Sun Nov 17 21:19:46 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Renegade Python Distribution's documentation!
========================================================

Contents:

.. toctree::
   :maxdepth: 2

   intro
   setupOperatingSystem
   softwareBuilds


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

Todo
====
.. todolist::

