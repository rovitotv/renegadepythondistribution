===============
Software Builds 
===============

We recommend following these steps in order because each piece of software is
dependent on the previous software.  During the software build process we
will try and explain our logic for doing things in this order. Each software
component has an external link that you can click on and save to the 
appropriate *~/PythonBuild/SoftwareTarballs* folder.

Please note the *~* character means your user's home directory from a bash shell
perspective.  In some cases we use the environment variable *$HOME* instead of
the *~*.  For the $HOME case we recommend you hit the tab key after typing so
bash fully expands the variable and that file is guaranteed to be present on
the file system.  

ATLAS/LAPACK
------------

.. warning:: This step is only required on Linux.  Apple ships their own accelerate framework.

ATLAS is Automatically Tuned Linear Algebra Software which is a software
library for linear algebra.  Since ATLAS has been around for some time it 
provides a mature open source implementation of BLAS APIs for C and Fortran77.
In a nutshell ATLAS and LAPACK are the math back ends that do the heavy lifting
for NumPy and SciPy.  ATLAS covers most of the needed math functionality for
NumPy and SciPy then LAPACK picks up the remaining functionality.  First
download `LAPACK 3.4.2 <http://www.netlib.org/lapack/lapack-3.4.2.tgz>`_ and
move to the directory *~/PythonBuild/SoftwareTarballs*.  Then download
`ATLAS 3.10.1 <http://sourceforge.net/projects/math-atlas/files/Stable/3.10.1/>`_ and move to the directory *~/PythonBuild/SoftwareTarballs*. Now enter these
commands to build ATLAS and LAPACK::

        cd ~/PythonBuild/builds
        tar xvfj ../SoftwareTarballs/atlas3.10.1.tar.bz2
        cd ATLAS
        mkdir build
        cd build
        ../configure -Fa alg -fPIC --with-netlib-lapack-tarfile=$HOME/PythonBuild/SoftwareTarballs/lapack-3.4.2.tgz
        make

.. warning:: After the configure step you should see a message "DONE configure"
    if you don't make sure the LAPACK tarfile is correct, be sure to tab out
    the $HOME and the file actually exists!

This command will use the LAPACK that you downloaded earlier and build ATLAS
and LAPACK together.  When building ATLAS try not to run other CPU intensive
programs as ATLAS is trying to tune itself for your system.  ATLAS will run
several tests as the build progresses. Be patient as it takes several minutes
to build ATLAS/LAPACK, on slower computers it can take a few hours. After the
build is complete change directory to lib and make the shared libraries::

        cd lib
        make shared
        make ptshared

I am not sure what to do with the shared libraries but we have them just in
case.  Shared libraries could possibly make the system easier to upgrade 
latter.  Now copy the ATLAS/LAPACK libraries to *~/PythonBuild/lib* directory::

        mkdir ~/PythonBuild/lib
        cp *.a *.so ~/PythonBuild/lib

At this point ATLAS/LAPACK are built and you have established a good foundation
for array computing.  
 
Python
------

What good would a Python Distribution be with out Python?  Not very good indeed
so here are step by step instructions to build Python.  First download `Python 2.7.6 <http://python.org/ftp/python/2.7.6/Python-2.7.6.tgz>`_ and move to
*~/PythonBuild/SoftwareTarballs* directory. Next enter the following commands
in a terminal to perform the build of Python::

        cd ~/PythonBuild/builds
        tar xvfz ../SoftwareTarballs/Python-2.7.6.tgz
        cd Python-2.7.6
        ./configure --enable-shared --prefix=$HOME/PythonBuild/
        make
        make install

Note on the configure command the user is not permitted to use the *~*
character so we use *$HOME* instead. After the end of the first make you should
see a message that says "Python build finished, but the necessary bits to build
these modules were not found".  On my machines the modules that were not built
are bsddb185, dl, gdbm, imageop, sunaudiodev which are all modules that are not
needed for S&E purposes.  The final *make install* command will install Python
in the *~/PythonBuild* directory and will create a bin, include, and share
directories then stash several other files in the already existing lib
directory.  Now in the terminal window you are building all this stuff in set
your path with the command::

        export PATH=$HOME/PythonBuild/bin:$PATH

Now when you type the command::

        which python

The shell should return *~/PythonBuild/bin/python* and when you actually
issue the *python* command the output should look like this::

        Python 2.7.6 (default, Nov 17 2013, 23:10:06) 
        [GCC 4.4.7 20120313 (Red Hat 4.4.7-3)] on linux2
        Type "help", "copyright", "credits" or "license" for more information.
        >>> 

Then type *quit()* to get back to the bash prompt, please note the version
number which should be 2.7.6 as default CentOS 6.4 comes with Python 2.6.6. Be
warned that if you switch terminal windows and your path is not set correctly
the build won't work.  To prevent this you might want to permanently set 
your path in .bashrc, use google for specifics.

Cython
------

The Cython programming language is a superset of Python with a foreign
function interface for invoking C/C++ routines and the ability to declare the
static type of subroutine parameters and results, local variables, and class
attributes.  In a nut shell Cython is a compiled language that generates 
CPython extension modules.  These extension modules can then be loaded and
used by regular Python code using the import statement.  Many S&E tools for
Python take advantage of Cython to gain speed.  First download `Cython 0.19.2 <http://cython.org/release/Cython-0.19.2.tar.gz>`_ from cython.org then move to
*~/PythonBuild/SoftwareTarballs*.  Next use the following commands to build
and install Cython::

        cd ~/PythonBuild/builds
        tar xvfz ../SoftwareTarballs/Cython-0.19.2.tar.gz
        cd Cython-0.19.2
        python setup.py install

NumPy requires Cython to be installed.


NumPy
-----

NumPy is an extension to the Python programming language which adds support for
large, multi-dimensional arrays and matrices, along with a large library of
high-level mathematical functions to operate on these arrays.  It is the heart
of the Python S&E stack and required for most scientific applications.  First
download `NumPy 1.8.0
<http://sourceforge.net/projects/numpy/files/NumPy/1.8.0/numpy-1.8.0.tar.gz/download>`_
from sourceforge.net and move to the *~/PythonBuild/SoftwareTarballs*
directory, then enter these commands to build NumPy::

        export BLAS=$HOME/PythonBuild/lib/libcblas.a
        export LAPACK=$HOME/PythonBuild/lib/liblapack.a
        export ATLAS=$HOME/PythonBuild/lib/libatlas.a
        cd ~/PythonBuild/builds/
        tar xvfz ../SoftwareTarballs/numpy-1.8.0.tar.gz
        cd numpy-1.8.0
        python setup.py build
        python setup.py install --prefix=$HOME/PythonBuild
        cd $HOME/PythonBuild

.. warning:: Don't forget to press the tab key *(aka tabbing out)* the lines 
             with the $HOME in them to insure the path really exists. 

.. warning:: On Mac OS X 10.8.5 you don't have to perform any of the export commands
             as NumPy will build against Apple Accelerate framework.

This will build and then install NumPy in
*~/PythonBuild/lib/python2.7/site-packages/numpy*.  The last cd will change
directory out of the NumPy directory to prepare for the testing step as NumPy
is designed to not allow users to test NumPy while in the NumPy directory.

SciPy
-----

SciPy is a computing environment and open source ecosystem of software for the 
Python programming language used by scientists and engineers doing 
scientific computing.  SciPy is a open source library of algorithms and
mathematical tools that form a core element of the SciPy environment for
technical computing.  SciPy contains modules for optimization, linear algebra,
integration, interpolation, special functions, FFT, signal, and image
processing.  Please make sure all the steps for NumPy have been followed
so the exports are setup properly to ATLAS, LAPACK, and BLAS. First download
`SciPy 0.13.1 <http://sourceforge.net/projects/scipy/files/scipy/0.13.1/scipy-0.13.1.tar.gz/download>`_ from sourceforge.net then move to 
*~/PythonBuild/SoftwareTarballs*.  Next use the following commands to build
and install SciPy::

        cd ~/PythonBuild/builds
        tar xvfz ../SoftwareTarballs/scipy-0.13.1.tar.gz
        cd scipy-0.13.1
        python setup.py build
        python setup.py install --prefix=$HOME/PythonBuild

SciPy will take some time to build and it produces a large number of warnings
during the build process.  Don't panic as we will test both NumPy and SciPy
latter on in this guide to make sure they are working appropriately. 

ZeroMQ/pyzmq
------------

ZeroMQ (also known as 0MQ, ZMQ) is a high performance asynchronous messaging
library aimed at use in scalable distributed or concurrent applications.  It
provides a message queue, but unlike message-oriented middleware, a ZeroMQ
system can run without a dedicated message broker.  The library is designed to
have a familiar socket-style API.  pyzmq is the software bindings for ZeroMQ
both of which are used by iPython which is built latter on in this guide.
First download `ZeroMQ 3.2.4 <http://download.zeromq.org/zeromq-3.2.4.tar.gz>`_
from zeromq.org then move to *~/PythonBuild/SoftwareTarballs*. Next use the
following commands to build and install ZeroMQ::

        cd ~/PythonBuild/builds
        tar xvfz ../SoftwareTarballs/zeromq-3.2.4.tar.gz
        cd zeromq-3.2.4
        ./configure --prefix=$HOME/PythonBuild/
        make
        make install

After ZeroMQ is installed we will install the Python bindings known as pyzmq by
following these steps::

        cd ~/PythonBuild/builds
        tar xvfz ../SoftwareTarballs/pyzmq-13.1.0.tar.gz
        cd pyzmq-13.1.0
        python setup.py configure --zmq=$HOME/PythonBuild
        python setup.py install

Sphinx
------

Sphinx is a collection of Python programs that are used to document
Python/C/C++ projects.  It is used for this guide that you are reading! Sphinx
requires several dependencies Pygments, setuptools, MarkupSafe, Jinja2, and
docutils.  This section of the guide will walk you through installing all the
dependencies then install Sphinx. First download `Pygments 1.6
<https://pypi.python.org/pypi/Pygments/1.6>`_ from pypi.python.org then move to
*~/PythonBuild/SoftwareTarballs*.  Next use the following commands to build and
install Pygments::

        cd ~/PythonBuild/builds
        tar xvfz ../SoftwareTarballs/Pygments-1.6.tar.gz
        cd Pygments-1.6
        python setup.py install

Next download `setuptools 1.4 <https://pypi.python.org/pypi/setuptools/1.4/>`_
from pypi.python.org then move to *~/PythonBuild/SoftwareTarballs*. Next use
the following commands to build and install setuptools::

        cd ~/PythonBuild/builds
        tar xvfz ../SoftwareTarballs/setuptools-1.4.tar.gz
        cd setuptools-1.4
        python setup.py install


Next download `MarkupSafe 0.18
<https://pypi.python.org/pypi/MarkupSafe/0.18/>`_ from pypi.python.org then
move to *~/PythonBuild/SoftwareTarballs*. Next use the following commands to
build and install setuptools::

        cd ~/PythonBuild/builds
        tar xvfz ../SoftwareTarballs/MarkupSafe-0.18.tar.gz
        cd MarkupSafe-0.18
        python setup.py install

Next download `Jinja2 2.7.1 <https://pypi.python.org/pypi/jinga2/2.7.1>`_ from
pypi.python.org then move to *~/PythonBuild/SoftwareTarballs*. Next use the
following commands to build and install Jinja::

        cd ~/PythonBuild/builds
        tar xvfz ../SoftwareTarballs/Jinja2-2.7.1.tar.gz
        cd Jinja2-2.7.1
        python setup.py install

Next download `docutils 0.11 <http://prdownloads.sourceforge.net/docutils/docutils-0.11.tar.gz?download>`_ from sourceforge.net then move to
*~/PythonBuild/SoftwareTarballs*.  Next use the following commands to build
and install docutils::

        cd ~/PythonBuild/builds
        tar xvfz ../SoftwareTarballs/docutils-0.11.tar.gz
        cd docutils-0.11
        python setup.py install

Finally Sphinx can be installed by downloading `sphinx 1.2b3
<https://pypi.python.org/pypi/Sphinx/1.2b3>`_ from pypi.python.org then move to
*~/PythonBuild/SoftwareTarballs*.  Next us the following commands to build and
install Sphinx::

        cd ~/PythonBuild/builds
        tar xvfz ../SoftwareTarballs/Sphinx-1.2b3.tar.gz
        cd Sphinx-1.2b3
        python setup.py install

.. note:: If you are connected to a network all you need is setuptools and
   Sphinx the other dependencies should automatically download from the pypi 
   site.

Nose
----

Nose extends unit test to make testing easier.  In detail Nose extends the test
loading and running features of unit test, making it easier to write, find and
run tests.  Nose is used to test NumPy and SciPy in particular. First download
`nose 1.3.0 <https://pypi.python.org/pypi/nose/1.3.0>`_ from pypi.python.org
then move to *~/PythonBuild/SoftwareTarballs*.  Next use the following
commands to build and install Nose::

        cd ~/PythonBuild/builds
        tar xvfz ../SoftwareTarballs/nose-1.3.0.tar.gz
        cd nose-1.3.0
        python setup.py install


iPython
-------

iPython is a powerful interactive shell that supports tab completion, rich
history, enhanced introspection, etc.   A complete Python S&E environment would
be incomplete without iPython.  iPython uses a complete readline environment
which does not ship with OS X.  To install readline on Max OS X only perform
the following command::

    easy_install readline


Next download `iPython 1.1.0
<https://pypi.python.org/pypi/ipython/1.1.0>`_ from pypi.python.org then move
to *~/PythonBuild/SoftwareTarballs*.  Next us the following commands to build
and install iPython::

        cd ~/PythonBuild/builds
        tar xvfz ../SoftwareTarballs/ipython-1.1.0.tar.gz
        cd ipython-1.1.0
        python setup.py install

Then to test iPython execute the command "iptest" which should produce a
bunch of tests with several dots on the screen.  The tests take a while to run
so please be patient! On my computer I get the following output after the
tests are run:: 

        Test suite completed for system with the following information:
        {'codename': 'An Afternoon Hack',
         'commit_hash': '7c2ea3a',
         'commit_source': 'installation',
         'default_encoding': 'UTF-8',
         'ipython_path': '/home/rovitotv/PythonBuild/lib/python2.7/site-packages/IPython',
         'ipython_version': '1.1.0',
         'os_name': 'posix',
         'platform': 'Linux-2.6.32-358.el6.x86_64-x86_64-with-centos-6.4-Final',
         'sys_executable': '/home/rovitotv/PythonBuild/bin/python',
         'sys_platform': 'linux2',
         'sys_version': '2.7.6 (default, Nov 18 2013, 10:01:32) \n[GCC 4.4.7 20120313 (Red Hat 4.4.7-3)]'}

        Tools and libraries available at test time:
           curses cython jinja2 numpy pexpect pygments sphinx sqlite3 zmq

        Tools and libraries NOT available at test time:
           azure matplotlib oct2py pymongo qt rpy2 tornado wx wx.aui

        Ran 12 test groups in 128.474s

        Status:
        OK

The pertinent part of the test is status is OK.

Matplotlib
----------

.. warning:: Currently this section doesn't work on Mac OS X only Linux.


Matplotlib is a program that is used to create graphs and charts.  It is useful
for data analysis.  The instructions in this guide will build Matplotlib with
the Tk back end.  Matplotlib requires python-dateutil 2.2 so follow these
instructions to install python-dateutil::


        cd ~/PythonBuild/builds
        tar xvfz ../SoftwareTarballs/python-dateutil-2.2.tar.gz
        cd python-dateutil-2.2
        python setup.py install

Matplotlib requires PyParsing so follow these instructions to install 
PyParsing::

        cd ~/PythonBuild/builds
        tar xvfz ../SoftwareTarballs/pyparsing-2.0.1.tar.gz
        cd pyparsing-2.0.1
        python setup.py install

.. note:: If connected to the internet then by installing Matplotlib it will
          automatically install python-dateutil-2.2 and pyparsing from pypi.

Follow these steps to build then install matplotlib::

        cd ~/PythonBuild/builds
        tar xvfz ../SoftwareTarballs/matplotlib-1.3.1.tar.gz
        cd matplotlib-1.3.1
        python setup.py build
        python setup.py install

One way to test the build of Matplotlib is to run iPython with the --pylab
flag which loads NumPy, SciPy, and matplotlib. After iPython starts it should
state "Using matplotlib backend: TkAgg


Testing
-------

Please run the unit tests for NumPy and SciPy to make sure you got a
clean build.  It might take a few minutes but it could save you hours of 
debug time latter down the road.

Test Numpy
^^^^^^^^^^

To test NumPy use the following commands::

        cd ~
        python -c "import numpy as np; np.test('full')"

The tests will take some time and you will see several dots on your screen, one
dot for each test. On my computer I get the following output after the tests
are run::

        Ran 5001 tests in 191.633s

        OK (KNOWNFAIL=5, SKIP=2)

Test SciPy
^^^^^^^^^^

To test SciPy use the following commands::

        cd ~
        python -c "import scipy as sp; sp.test('full')"

The tests will take some time and you will see several dots on your screen, one
dot for each test.  Several deprecation warnings appear on the screen along
with some S and K output, don't panic!  On my computer I get the following
output after the tests are run::

       Ran 9998 tests in 436.285s

       OK (KNOWNFAIL=133, SKIP=324)


Image Processing Software (Optional)
------------------------------------

If you do image processing you will want to follow the instructions in this
section where we install scikit-image for image processing fun! 

SciKit-Image
^^^^^^^^^^^^

SciKit-Image is the latest and greatest image processing algorithms in the S&E
Python toolchain.  It requires FreeImage which provides extra graphic file
support. To install FreeImage first download `FreeImage 3154
<http://downloads.sourceforge.net/freeimage/FreeImage3154.zip>`_ then move the
files into the directory *~/PythonBuild/SoftwareTarballs*.  FreeImage uses a
non standard build process, for Linux first copy the make file with the
following commands::

        cd ~/PythonBuild/builds
        unzip ../SoftwareTarballs/FreeImage3154.zip
        cd FreeImage
        cp Makefile.gnu Makefile.rpd

Now edit the make files and change the general configuration
variables at the top of each make file to look like below::

        DESTDIR ?= /home/rovitotv/PythonBuild
        INCDIR ?= $(DESTDIR)/include
        INSTALLDIR ?= $(DESTDIR)/lib

Where /home/rovitotv is your home directory on Linux.

For Mac OS X the make file is outdated so use the make file provided from this
guide, see `stackoverflow.com
<http://stackoverflow.com/questions/19080303/how-to-compile-freeimage-on-mac-os-x-10-8>`_
for more information.  First copy the make file with the following commands::

        cd /Volumes/RenegadePythonDistribution/builds/
        unzip ../SoftwareTarballs/FreeImage3154.zip
        cd FreeImage
        cp /Volumes/RenegadePythonDistribution/source/FreeImageMakefileOSX.rpd Makefile.rpd

Now edit Makefile.rpd change the prefix on line 49 to
/Volumes/RenegadePythonDistribution.  

Now execute the following commands::

        make -f Makefile.rpd
        sudo make -f Makefile.rpd install

.. todo:: Why the sudo command on the make install above?  We should be
          able to setup permissions the correct way so this is not required.

After that is complete you should see libfreeimage.* in *~/PythonBuild/lib*.
Finally we get to install scikit-image, to do so download `scikit-image 0.9.3
<https://pypi.python.org/pypi/scikit-image>`_ then use the following commands
to build and install:: 

        cd ~/PythonBuild/builds
        tar xvfz ../SoftwareTarballs/scikit-image-0.9.3.tar.gz
        cd scikit-image-0.9.3
        python setup.py install --prefix=$HOME/PythonBuild

After the build and install is complete then run the scikit-image unit tests
with the following command::

        cd ~
        python -c "import skimage;skimage.test()"

.. warning:: Currently the scikit-image test is failing, the reason for the 
    failure is because FreeImage has changed its API.  Don't
    panic!!!! The author of this guide posted a question about the `issue
    <https://github.com/scikit-image/scikit-image/pull/822>`_ and got a fast
    response with a fix.  The easiest thing to do is to ignore the test failure
    or you can apply a similar patch to the file
    ~/PythonBuild/builds/scikit-image-0.9.3/skimage/io/tests/test_freeimage.py
    change line 97 and line 103 to "assert meta[1][('EXIF_MAIN',
    'Software')].startswith('I')" then rerun the test and everything should
    pass.

OpenCV
^^^^^^

OpenCV is a set of popular computer vision algorithms which have excellet python
bindings.  OpenCV requires cmake to be used in the build process, I recommend
using the CMake GUI as the search feature is better.  Use the following
commands to extract the archive::

        cd ~/PythonBuild/builds
        unzip ../SoftwareTarballs/opencv-2.4.8.zip
        cd opencv-2.4.8
        mkdir build

Now run the CMake GUI and set the source directory to ~/PythonBuild/builds/opencv-2.4.8 and
the build directory to ~/PythonBuild/builds/opencv-2.4.8/build. 

.. warning:: Run the cmake GUI from the terminal with the evnironment variables for CC
     and CXX set to gcc and g++ respectively.  Otherwise cmake will use the clang 
     compiler on Mac OS X.

Next click on the "Configure" button, I generally use standard makefiles on
both Linux and Mac OS X.  After the configure process is complete type "python"
in the search field and set the following variables::

    INSTALL_PYTHON_EXAMPLES:    ON
    PYTHON_EXECUTABLE: /home/rovitotv/PythonBuild/bin/python
    PYTHON_INCLUDE_DIR: /home/rovitotv/PythonBuild/include/python2.7
    PYTHON_LIBRARY: /home/rovitotv/PythonBuild/lib/libpython2.7.a
    PYTHON_NUMPY_INCLUDE_DIR: /home/rovitotv/PythonBuild/lib/python2.7/site-packages/numpy/core/include
    PYTHON_PACKAGES_PATH: /home/rovitotv/PythonBuild/lib/python2.7/site-packages

Then using the search field set the following additional values::

    CMAKE_INSTALL_PREFIX: /home/rovitotv/PythonBuild
    EXECUTABLE_OUTPUT_PATH: /home/rovitotv/PythoinbBuild/bin

To build the OpenCV software click on the "Generate" button then execute the
command "make".  After the make is complete you should see output that includes
"BUILD SUCCESSFUL".  Then as typical with Unix software run "make install".
For Python and OpenCV to work together several paths have to be set correctly. To
set these paths I use a shell script called StartMeUp.sh::

        # Sets environment variables
        # use source on this file
        export PATH=/Volumes/RenegadePythonDistribution/bin:$PATH
        export DYLD_LIBRARY_PATH=/Volumes/RenegadePythonDistribution/lib:/Volumes/RenegadePythonDistribution/lib/python2.7/site-packages:$DYLD_LIBRARY_PATH
        export PYTHONPATH=/Volumes/RenegadePythonDistribution/lib/python2.7/site-packages/:/Volumes/RenegadePythonDistribution/lib:$PYTHONPATH


Execute the file with the source command "source StartMeUp.sh".  


    
