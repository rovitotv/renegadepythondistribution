Renegade Python Distribution is a set of simple instructions that will allow
a user to build their own version of Python and the full S&E stack including
NumPy, SciPy, Cython, iPython, Sphinx, Nose, pyzeromq, etc.  As of this release
the only operating system officially supported is CentOS 6.4 (so RedHat
Enterprise Linux 6.4 should work) but future versions will include Ubuntu and
Mac OS X.  This project is mostly for educational purposes.  The primary 
author, Todd V. Rovito, appreciates feedback and pull requests.  Thanks!

How to build Python!
How to build NumPy!
How to build SciPy!

We call it Renegade Python Distribution because you will have to be a little
bit of a Renegade to build your own Python instead of using one of the many
pre-built distributions like Anaconda or Enthought.  The advantage of building
your self is you get great performance with ATLAS (because it tunes itself for
your specific computer) and you can customize your Python Distribution to meet
your needs.  This guide doesn't take long to follow about one hour including
compile time.


Release Notes
=============

For releases look at the downloads section <https://bitbucket.org/rovitotv/renegadepythondistribution/downloads>


On 11/20/2013 the first release was made 0.1 <https://bitbucket.org/rovitotv/renegadepythondistribution/downloads> which just includes instructions
for CentOS 6.4.  
