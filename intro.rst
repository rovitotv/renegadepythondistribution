============
Introduction
============

Renegade Python Distribution is a set of simple instructions that will allow
a user to build their own version of Python and the full S&E stack including
NumPy, SciPy, Cython, iPython, Sphinx, Nose, pyzeromq, etc.  As of this release
the only operating system officially supported is CentOS 6.4 (so RedHat
Enterprise Linux 6.4 should work) but future versions will include Ubuntu and
Mac OS X.  This project is mostly for educational purposes.  The primary 
author, Todd V. Rovito, appreciates feedback and pull requests.  Thanks!

Versions
--------

Versions of the software that are covered in this guide include:

* OS: CentOS 6.4
* LAPACK: lapack-3.4.2.tgz
* ATLAS: 3.10.1
* Python: 2.7.6
* NumPy: 1.8.0
* SciPy: 0.13.1
* Cython: 0.19.2
* ZeroMQ: 3.2.4
* pyzmpq: 13.1.0
* Sphinx: 1.2b3   
        * Pygments: 1.6
        * setuptools: 1.4
        * Jinja2: 2.7.1
        * docutils: 0.11
* nose: 1.3.0
* iPython: 1.1.0
* matplotlib: 1.3.1
        * pyparsing: 2.0.1
        * python-dateutil: 2.2

The stuff below is optional for image processing:

* scikit-image: 0.9.3
        * freeimage: 3154
