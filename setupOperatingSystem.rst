======================
Operating System Setup
======================

This section outlines the steps needed to get the operating system setup and
to make sure all the dependencies are ready to go.

CentOS 6.4
----------

CentOS is a source compile of the RedHat Enterprise Linux source code so these
instructions should work with RHEL as well.  It would not be a shock if these
instructions work for the CentOS/RHEL 6.X series but I have only tested with
6.4.  To make things easy one might want to try the instructions in a 
virtual machine first just to minimize risk.  

Installation
^^^^^^^^^^^^

I selected a "Software Development" installation choice so the compilers gcc,
gfortran, and g++ will be installed.  After installation you can check to 
make sure each compiler is installed by typing::

        gcc
        gfortran
        g++

at a terminal. For each command you should get the following output::

        gcc: no input files
        gfortan: no input files
        g++: no input files

If you are missing any of these compilers use the yum command::

        sudo yum install gcc
        sudo yum install gfortran
        sudo yum install g++

Python makes heavy use of TCL/TK so make sure the development packages are
installed::

        sudo yum install tcl-devel
        sudo yum install tk-devel

One final note make sure you don't have fortran 77 installed that will confuse
the build process we are going for a build that uses gcc/gfortran which is
fortran 95.  To find out if you have fortran 77 installed type::

        g77

hopefully you will get "bash: g77: command not found" if the command exists
consider removing the program from your system.


Directory Setup
^^^^^^^^^^^^^^^

In my home directory I setup a directory called *PythonBuild*::

        cd ~
        mkdir PythonBuild

It might be a good idea to add a number at the end of the directory to create
a version system.  Inside of the *PythonBuild* directory I create two more
directories one called *SoftwareTarballs* to hold all my downloads of software
and another directory called *builds*, the *PythonBuild* directory will get
populated fast as software is built::

        mkdir ~/PythonBuild/SoftwareTarballs
        mkdir ~/PythonBuild/builds



Ubuntu
------

Ubuntu is a very popular OS and I should be able to get a version going for
Ubuntu in the near future.

.. todo:: add setup for Ubuntu and test software build instructions

Mac OS X 10.8.5
---------------

My Macs run OS X 10.8.5 this guide has not been tested outside of that
environment.  The first step is to download XCode through the Mac App store and
make sure XCode is up to date at the latest release.  Second install `gfortran
and gcc 4.2 <http://r.research.att.com/tools/gcc-42-5666.3-darwin11.pkg>`_ by
downloading the gfortran-42 file and installing like a normal mac app
install.  Then execute the following commands::

    $ export CC=gcc
    $ export CXX=g++
    $ export FFLAGS=-ff2c
    $ sudo ln -s /usr/bin/gfortran-4.2 /usr/bin/gfortran


Directory Setup
^^^^^^^^^^^^^^^

In my home directory I setup a directory called *PythonBuild*::

        cd ~
        mkdir PythonBuild

It might be a good idea to add a number at the end of the directory to create
a version system.  Inside of the *PythonBuild* directory I create two more
directories one called *SoftwareTarballs* to hold all my downloads of software
and another directory called *builds*, the *PythonBuild* directory will get
populated fast as software is built::

        mkdir ~/PythonBuild/SoftwareTarballs
        mkdir ~/PythonBuild/builds

Windows
-------

Somebody else will have to do a pull request for Windows because I don't
use Windows.

